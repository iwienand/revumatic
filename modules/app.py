#!/usr/bin/python3
import argparse
import functools
import locale
import os
import subprocess
import tuimatic

from modules.settings import settings
from modules.utils import lab
from modules import (
    actions,
    git,
    gitlab,
    kernel,         # noqa: F401
    list_filtered,
    list_history,
    list_mine,
    list_todo,
    menu_config,
    menu_main,
    mr,
    packages,
    screen,
    storage,
    theme,
    ui,
    utils,
    version,
)
all_screens = (mr, list_todo, list_mine, list_filtered, list_history, menu_config)


class RunError(Exception):
    pass


class App:
    CONFIG_DIR = '~/.revumatic'

    def __init__(self, plugin=None, plugin_args=None):
        self._on_start = []
        self._timers = {}
        self.loop = None
        locale.setlocale(locale.LC_ALL, '')
        if plugin is None:
            self.parse_cmd_line()
        else:
            self.plugin_init(plugin, plugin_args)
        self.init_settings()
        self.history = storage.HistoryStorage()
        self.init_ui()
        self.ui_check_config()

    def start(self):
        if not utils.is_lab_inited():
            try:
                utils.init_lab(gitlab.Gitlab(settings['global']['gitlab'],
                                            settings['global']['token']))
            except gitlab.AuthenticationError:
                self.ui_get_token('GitLab access denied.')
                return
        packages.registry.init()
        if ('rh_vpn' not in settings['global']
                and lab.me['public_email'].endswith('@redhat.com')):
            settings['global']['rh_vpn'] = True
        # settings could also be changed by the package's init
        settings.save()

        self.start_screen(self.args.cls, self.args)

    def parse_cmd_line(self):
        base_parser = argparse.ArgumentParser(add_help=False)
        base_parser.add_argument('-c', '--config-dir', default=self.CONFIG_DIR,
                                 help='specify a different config directory ' +
                                      '(default: {}).'.format(self.CONFIG_DIR))

        parser = argparse.ArgumentParser(parents=[base_parser])
        subparsers = parser.add_subparsers(dest='action', metavar='ACTION')
        subparsers.required = True

        # a dummy MR entry for help
        mr_help = 'MR_ID|MR_URL'
        sp_mr = subparsers.add_parser(mr_help, help='merge request id or merge request URL')
        mr.Screen.arg_help = sp_mr.format_help().replace(mr_help, '<url>')

        for mod in all_screens:
            cls = mod.Screen
            if cls.command is None:
                continue
            kwargs = {}
            help_text = cls.help
            help_epilog = cls.help_epilog
            if cls.help_raw:
                kwargs['formatter_class'] = argparse.RawDescriptionHelpFormatter
                if help_text:
                    help_text = help_text.replace('\\\n', '\n')
                if help_epilog:
                    help_epilog = help_epilog.replace('\\\n', '\n')
            sp = subparsers.add_parser(cls.command, help=help_text, **kwargs)
            cls.add_arguments(sp)
            cls.arg_help = sp.format_help()
            sp.epilog = help_epilog
            sp.set_defaults(cls=cls)

        try:
            import argcomplete
            argcomplete.autocomplete(parser)
        except ImportError:
            pass

        # first, parse only the common option
        self.args, rest = base_parser.parse_known_args()
        # if no action specified, start the main menu
        if len(rest) == 0:
            self.args.cls = menu_main.Screen
            self.args.all_screens = all_screens
            return
        # is the parameter a MR?
        if len(rest) == 1:
            if rest[0].startswith('http') or rest[0].isnumeric():
                self.args.action = 'mr'
                self.args.cls = mr.Screen
                self.args.mr_id = rest[0]
                return
        # hide our help hack
        if len(rest) and rest[0] == mr_help:
            raise RunError('Specify an actual MR id or MR URL, not a literal "{}" string.'.format(mr_help))
        # reparse with the complete parser
        self.args = parser.parse_args()

    def plugin_init(self, plugin, plugin_args):
        self.args = plugin_args or {}
        if isinstance(self.args, dict):
            self.args = self.to_args(self.args)
        self.args.cls = plugin
        if not hasattr(self.args, 'config_dir'):
            self.args.config_dir = self.CONFIG_DIR

    def init_settings(self):
        settings.init(os.path.expanduser(self.args.config_dir))
        all_themes = (utils.from_camel_case(a[:-5]) for a in dir(theme)
                                                    if a.endswith('Theme'))
        settings['global'].register('theme', tuple(all_themes), default='black',
                                    display_name='Color theme',
                                    help_text='''The color theme to use.''')
        packages.registry.register_settings()
        for mod in all_screens:
            mod.Screen.register_settings()

    def ui_get_token(self, msg):
        msg = (msg + ' Go to ' +
               'https://{}/-/profile/personal_access_tokens'
               .format(settings['global']['gitlab']) +
               ', select "api" scope and create a token.\nPaste the token:')
        old_token = settings['global']['token']
        self.edit_popup(functools.partial(self.ui_store_config, 'token'), msg, old_token)

    def ui_check_config(self):
        if not utils.is_lab_inited() and 'token' not in settings['global']:
            self.ui_get_token('GitLab Personal Access Token is needed.')
        else:
            settings.save()
            self.on_start(self.start)

    def ui_store_config(self, name, text):
        settings['global'][name] = text
        self.ui_check_config()

    def on_start(self, callback):
        if self.loop:
            callback()
        else:
            self._on_start.append(callback)

    def init_ui(self):
        class Widgets:
            pass

        tuimatic.display_common.UNPRINTABLE_TRANS_TABLE.update({
            9: ord(settings.format_tab[0]),
            1: ord(settings.format_tab[1]),
        })
        if not settings['format']['unicode-fullwidth']:
            tuimatic.text_format.disable_fullwidth()

        tuimatic.connect_global_signal('selection', self._widget_selection)
        tuimatic.connect_global_signal('child-focus', lambda obj, pos: self.update_help())

        self.screens = []
        self.widgets = Widgets()

        self.workplace = ui.Workplace(tuimatic.SolidFill(' '))

        self.default_header = ' {}: {}'.format(version.get_name(), version.get_desc())
        self.widgets.header = tuimatic.Text(self.default_header, wrap='ellipsis')
        self.widgets.header_status = tuimatic.Text('', wrap='clip')
        self.widgets.footer = tuimatic.Text('', wrap='clip')

        header_line = tuimatic.Columns((self.widgets.header,
                                     ('pack', self.widgets.header_status)))
        self.widgets.toplevel = tuimatic.Frame(tuimatic.AttrMap(self.workplace, 'std'),
                                            tuimatic.AttrMap(header_line, 'header'),
                                            tuimatic.AttrMap(self.widgets.footer, 'footer'))

        self.load_theme()

        tuimatic.connect_signal(settings, 'key-map-changed', self.update_help)
        tuimatic.connect_signal(self.workplace, 'focus', self.update_help)
        tuimatic.connect_signal(self.workplace, 'help', self.show_long_help)
        tuimatic.connect_signal(packages.registry, 'status-changed', self.set_header_status)
        self.workplace.refocus()

    def load_theme(self):
        self.scr = tuimatic.raw_display.Screen()
        theme_cls_name = utils.to_camel_case(settings['global']['theme']) + 'Theme'
        theme_class = getattr(theme, theme_cls_name)
        self.palette = theme_class(self.scr).palette

        self.focus_map = { p[0][2:]: p[0] for p in self.palette if p[0].startswith('a:') }
        self.focus_map[None] = 'a:std'
        self.passive_map = { p[0][2:]: p[0] for p in self.palette if p[0].startswith('p:') }
        self.passive_map[None] = 'p:std'
        self.highlight_map = { p[0][2:]: p[0] for p in self.palette if p[0].startswith('h:') }
        self.highlight_map[None] = 'h:std'

        self.overlay_map = { p[0]: 'dimmed' for p in self.palette }
        self.overlay_map[None] = 'dimmed'

    def update_help(self):
        self.widgets.footer.set_text(self.workplace.get_help())

    def show_long_help(self, data):
        if data:
            data = [('title', 'Keyboard shortcuts in this panel:\n\n')] + data
        # add help from the current screen
        if self.screens:
            screen = self.screens[-1]['screen']
            extra = screen.get_long_help()
            if extra:
                if data:
                    data.append('\n')
                data.extend(extra)
        # add help from all packages
        extra = list(packages.registry.collect(lambda x: x.get_extra_help()))
        if extra and data:
            data.append('\n')
        data.extend(extra)
        if not data:
            return
        data = [('title', '{} {}\n\n'.format(version.get_name(),
                                             version.get_version()))] + data
        self.view_popup(None, data)

    def _widget_selection(self, obj, started):
        actions.enable_actions(obj)
        if not obj._command_map.has_action(tuimatic.SELECTION_START):
            actdefs = [{ 'name': tuimatic.SELECTION_START, 'help': 'select text',
                         'long_help': 'text selection' },
                       { 'name': tuimatic.SELECTION_CANCEL, 'help': 'cancel',
                         'long_help': 'cancel text selection',
                         'enabled': False, 'prio': 100 },
                       { 'name': tuimatic.SELECTION_COPY, 'help': 'copy',
                         'long_help': 'copy text selection',
                         'enabled': False, 'prio': 100 },
                      ]
            if isinstance(obj, tuimatic.Editor):
                actdefs.append({ 'name': tuimatic.PASTE, 'help': 'paste',
                                 'long_help': 'paste previously copied text' })
                actdefs.append({ 'name': tuimatic.DELETE_LINE })
                if obj.keep_undo:
                    actdefs.append({ 'name': tuimatic.UNDO, 'help': 'undo' })
                    actdefs.append({ 'name': tuimatic.REDO, 'help': 'redo' })
            obj._command_map.add_actions(actdefs)
        obj._command_map.set_action_enabled(tuimatic.SELECTION_CANCEL, started)
        obj._command_map.set_action_enabled(tuimatic.SELECTION_COPY, started)
        self.update_help()

    def to_args(self, arg_dict):
        class Args:
            def __init__(self):
                for k, v in arg_dict.items():
                    setattr(self, k, v)
        return Args()

    def start_screen(self, screen_class, args=None, **kwargs):
        if args is None:
            args = self.to_args(kwargs)
        screen = screen_class(self, args)
        self.screens.append({ 'screen': screen })
        self.set_header(self.default_header)
        screen.start()

    def new_action_tracker(self, callback):
        return actions.ActionTracker(self.confirm, callback)

    def _wrap_popup_callback(self, callback):
        def wrapper(*args, **kwargs):
            self.update_help()
            self.loop.draw_screen()
            if callback:
                callback(*args, **kwargs)

        return wrapper

    def popup(self, callback=None, callback_args=None, **kwargs):
        return ui.Popup(self._wrap_popup_callback(callback),
                        self.workplace,
                        callback_args=callback_args,
                        overlay_attr_map=self.overlay_map,
                        **kwargs)

    def edit_popup(self, callback, text, edit_text='', allow_esc=False,
                   help_text=None, callback_args=None):
        """The callback gets the text in the 'text' keyword argument."""
        popup = self.popup(callback, callback_args)
        popup.set_default_button(True, allow_esc)
        widgets = [('pack', tuimatic.Text(text)),
                   ('pack', popup.new_edit('text', edit_text))]
        if help_text:
            widgets.append(('pack', tuimatic.TextSeparator()))
            widgets.append(('pack', tuimatic.Text(help_text)))
        widget = tuimatic.Pile(widgets)
        popup.start(widget)
        self.update_help()
        return popup

    def buttons_popup(self, callback, text, labels, allow_esc=False,
                      callback_args=None):
        """The callback gets the button in the 'button' keyword argument."""
        popup = self.popup(callback, callback_args, focus_map=self.focus_map)
        if allow_esc:
            popup.set_default_button(False, True)
        buttons = tuimatic.Columns([('pack', popup.new_button(label)) for label in labels],
                                dividechars=1)
        widget = tuimatic.Pile((('pack', tuimatic.Text(text)),
                             ('pack', buttons)))
        popup.start(widget)
        self.update_help()
        return popup

    def message_popup(self, callback, text, callback_args=None):
        popup = self.buttons_popup(callback, text, ('OK',), allow_esc=True,
                                   callback_args=callback_args)
        return popup

    def view_popup(self, callback, markup_or_textbox, callback_args=None):
        if not isinstance(markup_or_textbox, tuimatic.TextBox):
            markup_or_textbox = tuimatic.TextBox(tuimatic.Viewer(markup_or_textbox))
        popup = self.popup(callback, callback_args, height='full')
        widget = popup.register_widget('help', markup_or_textbox)
        popup.set_default_button(False, True)
        popup.start(tuimatic.AttrMap(widget, { 'help-desc': 'std' }))
        self.update_help()
        return popup

    def wait_popup(self, text):
        popup = self.popup()
        popup.start(tuimatic.Text(text))
        self.update_help()
        self.loop.draw_screen()
        return popup

    def wait_update(self, popup, text):
        popup.popup_widget.set_text(text)
        self.loop.draw_screen()

    def wait_log_popup(self, text):
        popup = self.popup()
        popup.log_widget = tuimatic.LogText(max_rows=10)
        popup.append = functools.partial(self.wait_log_add, popup)
        widget = tuimatic.Pile([('pack', tuimatic.Text(text)),
                             ('pack', popup.log_widget)])
        popup.start(widget)
        self.update_help()
        self.loop.draw_screen()
        return popup

    def wait_log_add(self, popup, text):
        popup.log_widget.add_text(tuimatic.TerminalFormat(text))
        self.loop.draw_screen()

    def progress_popup(self, done=100):
        popup = self.popup()
        progress = tuimatic.ProgressBar('std', 'progress', done=max(done, 1))
        popup.start(progress)
        self.update_help()
        self.loop.draw_screen()
        return popup

    def progress_update(self, popup, current):
        popup.popup_widget.set_completion(current)
        self.loop.draw_screen()

    def listbox_custom_popup(self, items, formatter=None, title=None, button_labels=None,
                             default_button_label=False, register_as=None,
                             callback=None, callback_args=None):
        """Popup with a ListBox that allows specialized behavior. Usually,
        callback is not set and an action is assigned to the returned
        listbox. This allows specialized behavior of the popup. The returned
        popup is not shown; call popup_show manually."""
        walker = ui.SmartListWalker(items, self.focus_map, self.passive_map,
                                    formatter=formatter)
        listbox = tuimatic.ListBox(walker)
        widgets = []
        if title:
            widgets.append(('pack', tuimatic.Text(title)))
        widgets.append(listbox)
        popup = self.popup(callback, callback_args, popup_widget=tuimatic.Pile(widgets),
                           height='pack', focus_map=self.focus_map)
        if button_labels:
            buttons = tuimatic.Columns([('pack', popup.new_button(label))
                                        for label in button_labels], dividechars=1)
            popup.popup_widget.contents.append((buttons, ('pack', None)))
        popup.set_default_button(default_button_label, True)
        if register_as:
            popup.register_widget(register_as, listbox)
        return popup, listbox

    def listbox_popup(self, callback, items, default_text=None, title=None,
                      callback_args=None):
        popup, listbox = self.listbox_custom_popup(items, title=title,
                                                   default_button_label=True,
                                                   register_as='text',
                                                   callback=callback,
                                                   callback_args=callback_args)
        if default_text is not None:
            listbox.focus_position = items.index(default_text)
        self.popup_show(popup)
        return popup

    def popup_show(self, popup):
        """Only used for popups created with popup_widget set (such as
        listbox_custom_popup), or to show the same popup again."""
        popup.show()
        self.update_help()

    def confirm(self, name, message, callback, args, reject_callback=None):
        def do_callback(button=None):
            if not button or not button.lower().startswith('yes'):
                if reject_callback:
                    reject_callback()
                return
            if button != 'yes':
                settings['global'].add('silent', name)
                settings.save()
            callback(*args)

        if name in settings['global']['silent']:
            callback(*args)
            return
        self.buttons_popup(do_callback, message,
                           ['~yes', '~no', '~Yes and don\'t ask again'],
                           allow_esc=True)

    def add_to_history(self, wait_popup, mr, body, minor=False):
        self.history.add_mr(mr, body,
                            self.wait_update,
                            (wait_popup, 'Waiting for a lock on history...'),
                            minor=minor)

    def redraw_screen(self):
        self.loop.screen.clear()

    def set_workplace(self, widget):
        self.screens[-1]['workplace'] = widget
        self.workplace.original_widget = widget
        self.update_help()

    def set_header(self, title):
        self.screens[-1]['title'] = title
        self.widgets.header.set_text(title)

    def set_header_status(self):
        status = list(packages.registry.collect(lambda x: x.get_status()))
        if status:
            status.insert(0, ' ')
            status.append(' ')
        self.widgets.header_status.set_text(status)

    def quit(self):
        self.screens.pop()
        if not self.screens:
            raise tuimatic.ExitMainLoop()
        screen = self.screens[-1]
        self.set_header(screen['title'])
        self.set_workplace(screen['workplace'])
        screen['screen'].restored()

    def get_quit_text(self):
        if len(self.screens) == 1:
            return 'quit'
        return 'back'

    def user_command(self, command, parm):
        if isinstance(parm, str):
            parm = (parm, )
        for p in parm:
            subprocess.Popen((command, p))

    def web_browser(self, url):
        self.user_command(settings['global']['web-browser'], url)

    def _add_timer(self, name, seconds, callback, user_args):
        self._timers[name] = self.loop.set_alarm_in(seconds, self._timer_handler,
                                                    (name, seconds, callback, user_args))

    def _timer_handler(self, loop, data):
        name, seconds, callback, user_args = data
        result = callback(*user_args)
        if result:
            self._add_timer(name, seconds, callback, user_args)
        else:
            del self._timers[name]

    def add_timer(self, name, seconds, callback, *user_args):
        """Will call the callback with user_args after the specified number
        of seconds. The callback may return True to rearm."""
        self.del_timer(name)
        self._add_timer(name, seconds, callback, user_args)

    def del_timer(self, name):
        if name not in self._timers:
            return
        self.loop.remove_alarm(self._timers[name])
        del self._timers[name]

    def keypress(self, key):
        if self.screens:
            screen = self.screens[-1]['screen']
            return screen.keypress(key)
        return False

    def run(self):
        self.loop = ui.EnhancedMainLoop(self.widgets.toplevel,
                                        screen=self.scr,
                                        palette=self.palette,
                                        on_start=self._on_start,
                                        handle_mouse=False,
                                        unhandled_input=self.keypress)
        self.loop.run()


def run(plugin=None, plugin_args=None):
    app = App(plugin, plugin_args)
    try:
        app.run()
    except (screen.ScreenError, git.GitError) as e:
        app.loop.screen.stop()
        raise RunError(str(e))
