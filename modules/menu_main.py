from modules import (
    menu_base,
    screen,
    utils,
)

class Screen(menu_base.MenuScreen):
    def __init__(self, app, args):
        super().__init__(app, args)
        self.screens = [m.Screen for m in args.all_screens if m.Screen.help]

    def add_actions(self, actions):
        actions.extend((
            { 'name': 'open', 'prio': 2, 'help': 'select' },
            { 'name': 'quit', 'prio': 0, 'help': self.app.get_quit_text(),
              'confirm': 'Do you want to exit Revumatic?' },
        ))

    def load_keys(self):
        return self.screens

    def format_item(self, key):
        return utils.capitalize(key.help)

    def get_help(self, key):
        return [('desc-sys', 'From command line:\n'),
                utils.removeprefix(key.arg_help, 'usage: ')]

    def _open(self, scr, kwargs={}):
        try:
            self.app.start_screen(scr, **kwargs)
        except screen.ScreenError as e:
            self.app.message_popup(None, str(e))

    def open_with_arg(self, scr, text=None):
        if text is None:
            return
        self._open(scr, { scr.ui_arg['name']: text })

    def open(self):
        scr = self.current_key
        if scr.ui_arg:
            if type(scr.ui_arg['type']) == tuple:
                self.app.listbox_popup(self.open_with_arg, scr.ui_arg['type'],
                                       title=scr.ui_arg['display_name'],
                                       callback_args=(scr,))
            elif scr.ui_arg['type'] == str:
                kwargs = {}
                if 'help' in scr.ui_arg:
                    kwargs['help_text'] = scr.ui_arg['help'].replace('\\\n', ' ')
                self.app.edit_popup(self.open_with_arg, scr.ui_arg['display_name'], '',
                                    allow_esc=True, callback_args=(scr,), **kwargs)
            return
        self._open(scr)

    def action(self, what, widget, size):
        if what == 'quit':
            self.app.quit()
        elif what == 'open':
            self.open()
